{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Fermions3d.GNYHeisenberg2023 where

import           Data.Text                            (Text)
import           Hyperion
import           Hyperion.Bootstrap.Main              (unknownProgram)

boundsProgram :: Text -> Cluster ()
boundsProgram p = unknownProgram p
