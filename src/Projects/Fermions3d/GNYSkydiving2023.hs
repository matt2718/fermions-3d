{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Fermions3d.GNYSkydiving2023 where

import qualified Blocks                             as Blocks
import qualified Blocks.Blocks3d                      as B3d
import qualified Bootstrap.Bounds.Spectrum            as Spectrum
import           Bootstrap.Bounds                     (BoundDirection (..))
import           Bootstrap.Math.Linear                ( Matrix, V, fromV, toV
                                                      , diagonalMatrix )
import qualified Bootstrap.Math.Linear                as BML
import           Bounds.Fermions3d.GNY                (ChannelType (..))
import qualified Bounds.Fermions3d.GNYSigPsiEps       as GNYSPE
import           Bounds.Scalars3d.ONRep               (ONRep (..))
import qualified Config
import           Control.Monad                        (void, when, forM_)
import           Control.Monad.IO.Class               (liftIO)
import           Control.Monad.Reader                 (asks, local)
import           Data.List                            (transpose, intercalate)
import           Data.List.Split                      (chunksOf)
import qualified Data.Map.Strict                      as Map
import qualified Data.Matrix.Static                   as M
import           Data.Ratio                           (approxRational)
import qualified Data.Set                             as Set
import           Data.Text                            (Text)
import           Data.Time.Clock                      (NominalDiffTime)
import           Hyperion
import           Bootstrap.Math.AffineTransform   (AffineTransform (..),
                                                       apply)
import           Hyperion.Bootstrap.Bound             (Bound (..), BoundFiles,
                                                       CheckpointMap, LambdaMap)
import qualified Hyperion.Bootstrap.Bound             as Bound
import           Hyperion.Bootstrap.DelaunaySearch    (DelaunayConfig (..),
                                                       delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main              (unknownProgram)
import           Hyperion.Bootstrap.OPESearch         (BilinearForms (..),
                                                       OPESearchConfig (..))
import qualified Hyperion.Bootstrap.OPESearch         as OPE
import qualified Hyperion.Bootstrap.Params            as Params
import qualified Hyperion.Bootstrap.SDPDeriv          as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.BFGS     as BFGS
import qualified Hyperion.Bootstrap.SDPDeriv.Jet2     as Jet2
import qualified Hyperion.Bootstrap.SDPDeriv.Newton   as Newton
import qualified Hyperion.Bootstrap.TiptopSearch      as TT
--import           Hyperion.Concurrent                  (doConcurrently)
import qualified Hyperion.Database                    as DB
import qualified Hyperion.Log                         as Log
import qualified Hyperion.Slurm                       as Slurm
import           Hyperion.Util                        (hour)
import           Linear.Metric                        (quadrance)
import           Linear.Vector                        ((*^), (^/), (^+^))
import           Numeric.Rounded                      (Rounded,
                                                       RoundingMode (..))
import           Projects.Fermions3d.Defaults         (defaultBoundConfig,
                                                       defaultDelaunayConfig,
                                                       defaultQuadraticNetConfig)
import           SDPB                                 (Params (..))
import qualified SDPB
import           Blocks.Delta
import           System.Directory                     (removePathForcibly)

import           Projects.Fermions3d.GNYSigPsiEpsTest2020 (mkBound6Dspp, defaultStartingPoint,
                                                          remoteGNYSigPsiEpsOPESearch)
import qualified Projects.Fermions3d.GNYSigPsiEpsTest2020 as GNY2020
import           Projects.Fermions3d.Delaunay2
import qualified Projects.Fermions3d.GNYData2020Chi35 as GNYData
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Typeable                          (Typeable)
import           GHC.TypeNats                           (KnownNat)
import           Projects.SkydivingRun                  (DynamicalSdpOutput(..), 
                                                         DynamicalDBOutput(..), 
                                                         defaultDynamicalConfig, 
                                                         DynamicalConfig(..),
                                                         BigFloat, 
                                                         runDynamicalSdpBound) 
import qualified Projects.Fermions3d.GNYN4Data        as N4
import qualified Projects.Fermions3d.GNYN8Data        as N8
import           System.FilePath.Posix             ((</>))

----------
srunDynamicalSdpPath :: FilePath
srunDynamicalSdpPath = "/home/mmitchell/fermions-3d/hyperion-config/expanse-msm/srun_dynamical_sdp.sh"

dynamicalSdpBoundLoop
  :: forall n a . (KnownNat n, RealFloat a, Show a, ToJSON a, FromJSON a, Typeable a)
  => BoundFiles
  -> DynamicalConfig n a
  -> V n a
  -> (V n a -> Bound Int GNYSPE.GNYSigPsiEps)
  -> Int
  -> Rational
  -> Job (V n a, Maybe Bool)
dynamicalSdpBoundLoop boundFiles dynConfig startPoint mkBound numIters tolerance = do
    maybeExistDyn <- DB.lookup prevDynamicalMap (numIters::Int, uniqJobKey)
    case maybeExistDyn of 
      Nothing -> go startPoint numIters dynConfig 
      Just dynDB -> go (extPara dynDB) numIters dynConfig { totalIterationCount = prevIterations dynDB
                                                          , prevGradient = Just $ bfgsGradient dynDB
                                                          , prevHessian = Just $ bfgsHessian dynDB
                                                          , prevExternalStep = Just $ extStep dynDB
                                                          }
  where
    uniqJobKey = Bound.jsonDir boundFiles
    prevDynamicalMap :: DB.KeyValMap (Int, FilePath) (DynamicalDBOutput n a)
    prevDynamicalMap = DB.KeyValMap "prevDynamicalOutput"
    go p 0 _ = pure (p, Nothing)
    go p n dynconfig = do
      dynSdpOut <- runDynamicalSdpBound srunDynamicalSdpPath boundFiles dynconfig p mkBound
      let p' = p + externalParamStep dynSdpOut 
      Log.info "dynamicalSdpBoundLoop: Computed" dynSdpOut
      Log.info "dynamicalSdpBoundLoop: Testing point" p'
      DB.insert prevDynamicalMap (numIters::Int, uniqJobKey) $
                DynamicalDBOutput 
                  { prevIterations = totalIterations dynSdpOut 
                  , bfgsGradient = gradientBFGS dynSdpOut 
                  , bfgsHessian  = hessianBFGS dynSdpOut
                  , extPara = p'
                  , extStep = externalParamStep dynSdpOut
                  }
      let dynconfig' = dynconfig  { totalIterationCount = totalIterations dynSdpOut
                                  , prevExternalStep = Just $ externalParamStep dynSdpOut 
                                  , prevGradient = Just $  gradientBFGS dynSdpOut
                                  , prevHessian  = Just $ hessianBFGS dynSdpOut
                                  , useExactHessian = (n == numIters || n `mod` 10 == 1)
                                  , dualityGapUpperLimit = 0.25
                                  }
      if
        | (SDPB.primalObjective (sdpbOutput dynSdpOut)) < 0
          -> pure (p, Just True)
        | quadrance (gradientBFGS dynSdpOut) < fromRational (tolerance * tolerance)
          -> pure (p, Just False)
        | (SDPB.terminateReason (sdpbOutput dynSdpOut)) == SDPB.UpdateSDPs 
          -> go p' (n-1) dynconfig'
        | otherwise -> pure (p, Nothing)

testDynamicalSdpGNY3d :: Int -> Rational -> Rational -> Rational
                      -> Job (V 6 (BigFloat 512), Maybe Bool)
testDynamicalSdpGNY3d nmax ngroup sigP sigPP = do
  Log.info "Starting search with params" (ngroup, sigP, sigPP)
  workDir <- newWorkDir (gnyNavBound externalStart)
  let gnyNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop gnyNavBoundFiles dynConfig externalStart gnyNavBound numIters tolerance
  where
    numIters = 300
    tolerance = 0.01
    dynConfig = (defaultDynamicalConfig 6)
      { bBoxMax = toV (0.08, 0.5, 1.0, -1.5, -1.5, -1.5)
      , bBoxMin = toV (2.0 , 1.5, 2.5,  1.5,  1.5,  1.5)
      , prevHessian = Just $ diagonalMatrix (toV (100.0, 100.0, 100.0, 100.0, 100.0, 100.0))
      }
    externalStart = fromRational <$> defaultStartingPoint ngroup
    gnyNavBound :: V 6 (BigFloat 512) -> Bound Int GNYSPE.GNYSigPsiEps
    gnyNavBound dimV = mkBound6Dspp ngroup nmax sigP sigPP (flip approxRational 1e-32 <$> dimV)

prettyPrintResult
  :: forall a b n . (KnownNat n, RealFloat a, Show a, Show b)
  => b -> V n a -> Maybe Bool -> Cluster()
prettyPrintResult pointSpec v mbAllowed = do
  let allowedMsg = case mbAllowed of
               Just True  -> "ALLOWED"
               Just False -> "DISALLOWED"
               Nothing    -> "UNKNOWN"
  Log.info allowedMsg pointSpec
  Log.info "Final point" v

------------ DELAUNAY (move this) ----------------
data SSpDelaunaySearchData = SSpDelaunaySearchData
  { nmax              :: Int
  , affine            :: AffineTransform 2 Rational
  , initialLambda     :: V 4 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 2 Rational]
  , initialAllowed    :: [V 2 Rational]
  , initialSearch     :: [V 2 Rational]
  , opeEllipse        :: Matrix 4 4 Rational
  , solverPrecision   :: Int
  , delaunayConfig    :: DelaunayConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  }

gnyFeasibleSPP :: V 2 Rational -> V 3 Rational -> Maybe (V 4 Rational)
               -> Rational -> Int -> GNYSPE.GNYSigPsiEps
gnyFeasibleSPP deltaSS deltaExts mLambda ngroup nmax = GNYSPE.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $
    Spectrum.setGaps
    [ (GNY2020.psiChannel, 2)
    , (GNY2020.chiChannel, 3.5)
    , (GNY2020.sigChannel, deltaSigPP)
    , (GNY2020.epsChannel, 3)
    , (GNY2020.sigTChannel, 2)
    ] $
    Spectrum.addIsolated GNY2020.sigChannel deltaSigP
    Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSPE.Feasibility mLambda
  , externalDims = GNYSPE.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaSigP, deltaSigPP) = fromV deltaSS
    (deltaPsi, deltaSig, deltaEps) = fromV deltaExts

gnyFeasibleEPP :: V 2 Rational -> V 3 Rational -> Maybe (V 4 Rational)
               -> Rational -> Int -> GNYSPE.GNYSigPsiEps
gnyFeasibleEPP deltaSS deltaExts mLambda ngroup nmax = GNYSPE.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $
    Spectrum.setGaps
    [ (GNY2020.psiChannel, 2)
    , (GNY2020.chiChannel, 3.5)
    , (GNY2020.sigChannel, 2.5)
    , (GNY2020.epsChannel, deltaEpsPP)
    , (GNY2020.sigTChannel, 2)
    ] $
    Spectrum.addIsolated GNY2020.epsChannel deltaEpsP
    Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSPE.Feasibility mLambda
  , externalDims = GNYSPE.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaEpsP, deltaEpsPP) = fromV deltaSS
    (deltaPsi, deltaSig, deltaEps) = fromV deltaExts

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

deltaExtToV' :: Bound prec GNYSPE.GNYSigPsiEps -> V 2 Rational
deltaExtToV' Bound { boundKey = GNYSPE.GNYSigPsiEps { spectrum = spec } } =
  toV (ds', ds'')
  where
    Fixed ds'  = Set.findMax $ (Spectrum.deltaIsolateds spec) Map.! GNY2020.sigChannel
    Fixed ds'' = (Spectrum.deltaGaps spec) Map.! GNY2020.sigChannel

deltaExtToVee :: Bound prec GNYSPE.GNYSigPsiEps -> V 2 Rational
deltaExtToVee Bound { boundKey = GNYSPE.GNYSigPsiEps { spectrum = spec } } =
  toV (de', de'')
  where
    Fixed de'  = Set.findMax $ (Spectrum.deltaIsolateds spec) Map.! GNY2020.epsChannel
    Fixed de'' = (Spectrum.deltaGaps spec) Map.! GNY2020.epsChannel

deltaExtToV4 :: Bound prec GNYSPE.GNYSigPsiEps -> V 4 Rational
deltaExtToV4 Bound { boundKey = GNYSPE.GNYSigPsiEps
                     { spectrum = spec
                     , externalDims = GNYSPE.ExternalDims {..}}
                   } =
  toV (deltaPsi, deltaSig, deltaEps, ds')
  where
    Fixed ds'  = Set.findMax $ (Spectrum.deltaIsolateds spec) Map.! GNY2020.sigChannel

deltaExtToV4e :: Bound prec GNYSPE.GNYSigPsiEps -> V 4 Rational
deltaExtToV4e Bound { boundKey = GNYSPE.GNYSigPsiEps
                     { spectrum = spec
                     , externalDims = GNYSPE.ExternalDims {..}}
                   } =
  toV (deltaPsi, deltaSig, deltaEps, de')
  where
    Fixed de'  = Set.findMax $ (Spectrum.deltaIsolateds spec) Map.! GNY2020.epsChannel

gnyDelaunaySearchOPEScan :: Rational -> SSpDelaunaySearchData -> V 3 Rational -> Cluster ()
gnyDelaunaySearchOPEScan nGroup SSpDelaunaySearchData{..} deltaExts =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine pointFromBound initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine pointFromBound (Just initialLambda)
  delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
     (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound)
  where
    pointFromBound = deltaExtToVee --deltaExtToV'
    bound v = Bound
      { boundKey = gnyFeasibleEPP v deltaExts (Just initialLambda) nGroup nmax
        -- gnyFeasibleSPP v deltaExts (Just initialLambda) nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
                                                , procGranularity = 32 }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]++
      [(p, Nothing)    | p <- initialSearch]

gnyCloudSearch :: Rational -> SSpDelaunaySearchData -> V 3 Rational -> V 3 Rational  -> Cluster ()
gnyCloudSearch nGroup SSpDelaunaySearchData{..} deltaExts dDeltaExts =
  --local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
  --       setSlurmPartition "compute") $ void $ do
  local (setJobType (MPIJob 1 64)  . setJobTime (16*hour) . setJobMemory "120G" .
         setSlurmPartition "shared") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV' initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV' (Just initialLambda)
  forConcurrently_ (transpose $ chunksOf 50 searchPts) $
    mapM_ $
    \(c,xdims) -> do
      searchStatus <- remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
                      (BilinearForms 1e-32 [(Nothing, opeEllipse)]) (bound c xdims)
      DB.insert cloudSearchPoints (xdims BML.++ c) (Just searchStatus)
      Log.info "Finished point" ( fromRational @Double <$> c
                                , fromRational @Double <$> xdims
                                , searchStatus)
  where
    bound v deltaExts' = Bound
      { boundKey = gnyFeasibleSPP v deltaExts' (Just initialLambda) nGroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax) --solverPrecision
      , solverParams = (Params.jumpFindingParams nmax)
        { precision = solverPrecision, procGranularity = 32 }
      , boundConfig = defaultBoundConfig
      }
    searchPts = [ (p, deltaExts ^+^ dx)
                | dx <- searchStar, p <- initialSearch]
    icosahedron :: [V 3 Rational]
    icosahedron = [ toV <$> [(0,dx2*x,dx3*y), (dx1*x,dx2*y,0), (dx1*y,0,dx3*x)]
                  | x <- [1,-1], y <- [1.618034,-1.618034] ] >>= id
    searchStar :: [V 3 Rational]
    searchStar = toV (0,0,0) : ((^/ 2) <$> icosahedron) ++ icosahedron
    (dx1,dx2,dx3) = fromV dDeltaExts
    cloudSearchPoints :: DB.KeyValMap (V 5 Rational) (Maybe Bool)
    cloudSearchPoints = DB.KeyValMap "cloudSearchPoints"

defaultSearchDataN2 :: SSpDelaunaySearchData
defaultSearchDataN2 = SSpDelaunaySearchData
  { nmax = 10
  , affine = AffineTransform
    { affineShift  = toV ( 3.5, 4.8 )
    , affineLinear = toV ( toV (1, 0)
                         , toV (0, 1.2)
                         )
    }
  , initialLambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
  , initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/uIFOg/Object_Qow0w3bOiW4eBkdkxMQRamPo361rRSy9FKGzLeQokYU/ck"
    -- Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/izLxI/Object_O48YmWSFdO5Xqfvxt0J36xq8etqSRvAGPxBTh0DsHAY/ck"
  , initialDisallowed = []
  , initialAllowed = []
  , initialSearch = [toV (s', 4.0) | s' <- [3.00, 3.01 .. 3.10] ++ [3.48, 3.49, 3.50, 3.51]]
  --, initialSearch = toV <$> filter (uncurry (<))
  --                  [ (s', s'') | s'  <- [3.1, 3.4, 4.0]
  --                              , s'' <- [4.0, 5.0] ]
  , opeEllipse = GNYData.gny2Sig25bilinearMatNmax10
  , solverPrecision = 960
  , delaunayConfig = defaultDelaunayConfig 20 300
  , jobTime = 16*hour
  , jobType = MPIJob 1 128--32
  , jobMemory = "0" -- "64G"
  }

defaultSearchDataN4 :: SSpDelaunaySearchData
defaultSearchDataN4 = defaultSearchDataN2
  { affine = AffineTransform
    { affineShift  = toV ( 3.5, 4.5 )
    , affineLinear = toV ( toV (1, 0)
                         , toV (0, 1)
                         )
    }
  , initialLambda = toV (-0.2482848139835859, -0.11776422706986912, -0.48795745971324284, -0.8284797351845291)
 -- toV (-0.213416515180156,-0.0759121945368979,-0.486030874077683,-0.844060047829415)
  , initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/GDfMr/Object_NGdsjl6tPo_Z5Zhdxf1MNE32Mjw17DKLdfKIWLSPDWo/ck"
  , initialSearch = toV <$> filter (uncurry (<))
                    [(s', s'') | s'  <- [3.2, 3.5, 3.9]
                               , s'' <- [3.6, 4.2] ]
  , opeEllipse = N4.nmax10OpeEllipse
  }

defaultSearchDataN8 :: SSpDelaunaySearchData
defaultSearchDataN8 = defaultSearchDataN2
  { affine = AffineTransform
    { affineShift  = toV ( 3.5, 4.5 )
    , affineLinear = toV ( toV (1, 0)
                         , toV (0, 1)
                         )
    }
  , initialLambda = toV (0.147354, -0.0359405, -0.444498, -0.880165)
  , initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/izlEJ/Object_t6koqdmF6Oo7TZwkMoyAT4QgdGSNJmMfBtmWIB03_f8/ck"
  , initialSearch = toV <$> filter (uncurry (<))
                    [(s', s'') | s'  <- [3.1, 3.5, 3.9]
                               , s'' <- [3.6, 4.2] ]
  , opeEllipse = N8.nmax10OpeEllipse
  }

setNmax :: Int -> SSpDelaunaySearchData -> SSpDelaunaySearchData
setNmax nm searchData = searchData
  { nmax = nm
  , initialCheckpoint = Nothing --checkpoints won't work for a different nmax
  }

withNewSearchPoints :: SSpDelaunaySearchData -> SSpDelaunaySearchData
withNewSearchPoints searchData = searchData
  { initialSearch = toV <$> --filter (uncurry (<))
                    [(s', s'') | s'  <- [2.95, 2.98, 3.02, 3.05, 3.1, 3.2]
                               , s'' <- [4.5] ]
  }
------------ END DELAUNAY ----------------------

------------ TIPTOP ----------------------------
defaultTiptopConfig :: Int -> Int -> TT.TiptopSearchConfig
defaultTiptopConfig nThreads nSteps = TT.TiptopSearchConfig
  { TT.heightResolution = 268435456
  , TT.widthResolution = 1e-30
  , TT.jumpResolution = 2
  , TT.tiptopExecutable = Config.scriptsDir </> "tiptop.sh"
  , TT.nSteps = nSteps
  , TT.nThreads = nThreads
  , terminateTime = Nothing
  }

data GNYTiptopConfig = GNYTiptopConfig
  { nmax              :: Int
  , affine            :: AffineTransform 4 Rational
  , initialLambda     :: V 4 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 4 Rational]
  , initialAllowed    :: [V 4 Rational]
  , initialSearch     :: [V 4 Rational]
  , opeEllipse        :: Matrix 4 4 Rational
  , solverPrecision   :: Int
  , tiptopConfig      :: TT.TiptopSearchConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  , jobPartition      :: Text
  }

checkBoundOutput :: Cluster (SDPB.Output) -> Cluster Bool
checkBoundOutput = fmap ((==SDPB.PrimalFeasibleJumpDetected) . SDPB.terminateReason)

gnyTiptopSearchOPEScanSig :: Rational -> GNYTiptopConfig -> Cluster ()
gnyTiptopSearchOPEScanSig nGroup GNYTiptopConfig{..} =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
         setSlurmPartition jobPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV4 initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV4 (Just initialLambda)
  let searchV = remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
                (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound
  -- let searchV = checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound
  pointsToRun <- if initialAllowed == [] && initialDisallowed == []
                 then Map.fromList <$> mapConcurrently (toMapEntry searchV) initialSearch
                 else return initialPts
  Log.info "Points to run" pointsToRun
  TT.tiptopSearchRegionPersistent delaunaySearchPoints tiptopConfig affine pointsToRun searchV
  where
    toMapEntry searchV v = do
      isAllowed <- searchV v
      return (v, Just isAllowed)
    bound v = Bound
      { boundKey = gnyFeasibleSPP sDims deltaExts (Just initialLambda) nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
                                                , procGranularity = 4} -- 8 }
      , boundConfig = defaultBoundConfig
      }
      where
        sDims = toV (w, 4.5)
        deltaExts = toV (x,y,z)
        (x,y,z,w) = fromV v
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]++
      [(p, Nothing)    | p <- initialSearch]

-- gnyBatchOPEScanSig :: Rational -> GNYTiptopConfig -> Cluster ()
-- gnyBatchOPEScanSig nGroup GNYTiptopConfig{..} =
--   local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
--          setSlurmPartition jobPartition) $ void $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtToV4 initialCheckpoint
--   lambdaMap     <- Bound.newLambdaMap affine deltaExtToV4 (Just initialLambda)
--   let searchV = remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
--                 (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound
--   -- let searchV = checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound
--   pointsToRun <- if initialAllowed == [] && initialDisallowed == []
--                  then Map.fromList <$> mapConcurrently (toMapEntry searchV) initialSearch
--                  else return initialPts
--   Log.info "Points to run" pointsToRun
--   TT.tiptopSearchRegionPersistent delaunaySearchPoints tiptopConfig affine pointsToRun searchV
--   where
--     toMapEntry searchV v = do
--       isAllowed <- searchV v
--       return (v, Just isAllowed)
--     bound v = Bound
--       { boundKey = gnyFeasibleSPP sDims deltaExts (Just initialLambda) nGroup nmax
--       , precision = solverPrecision
--       , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
--                                                 , procGranularity = 4} -- 8 }
--       , boundConfig = defaultBoundConfig
--       }
--       where
--         sDims = toV (w, 4.5)
--         deltaExts = toV (x,y,z)
--         (x,y,z,w) = fromV v
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]++
--       [(p, Nothing)    | p <- initialSearch]

gnyTiptopSearchOPEScanEp :: Rational -> GNYTiptopConfig -> Cluster ()
gnyTiptopSearchOPEScanEp nGroup GNYTiptopConfig{..} =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
         setSlurmPartition jobPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV4e initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV4e (Just initialLambda)
  let searchV = remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
                (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound
  -- let searchV = checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound
  pointsToRun <- if initialAllowed == [] && initialDisallowed == []
                 then Map.fromList <$> mapConcurrently (toMapEntry searchV) initialSearch
                 else return initialPts
  Log.info "Points to run" pointsToRun
  TT.tiptopSearchRegionPersistent delaunaySearchPoints tiptopConfig affine pointsToRun searchV
  where
    toMapEntry searchV v = do
      isAllowed <- searchV v
      return (v, Just isAllowed)
    bound v = Bound
      { boundKey = gnyFeasibleEPP sDims deltaExts (Just initialLambda) nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
                                                , procGranularity = 4} -- 8 }
      , boundConfig = defaultBoundConfig
      }
      where
        sDims = toV (w, 4)
        deltaExts = toV (x,y,z)
        (x,y,z,w) = fromV v
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]++
      [(p, Nothing)    | p <- initialSearch]

gnyTiptopSearchNoScan :: Rational -> GNYTiptopConfig -> Cluster ()
gnyTiptopSearchNoScan nGroup GNYTiptopConfig{..} =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
         setSlurmPartition jobPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV4 initialCheckpoint
  let searchV = checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound
  pointsToRun <- if initialAllowed == [] && initialDisallowed == []
                 then Map.fromList <$> mapConcurrently (toMapEntry searchV) initialSearch
                 else return initialPts
  Log.info "Points to run" pointsToRun
  TT.tiptopSearchRegionPersistent delaunaySearchPoints tiptopConfig affine pointsToRun searchV
  where
    toMapEntry searchV v = do
      isAllowed <- searchV v
      return (v, Just isAllowed)
    bound v = Bound
      { boundKey = gnyFeasibleSPP sDims deltaExts Nothing nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
                                                , procGranularity = 8 }
      , boundConfig = defaultBoundConfig
      }
      where
        sDims = toV (w, 4.5)
        deltaExts = toV (x,y,z)
        (x,y,z,w) = fromV v
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]++
      [(p, Nothing)    | p <- initialSearch]

cvCube :: [(Rational, Rational, Rational)]
cvCube = (0,0,0) : [(dx,dy,dz) | dx <- [-1,1], dy <- [-1,1], dz <- [-1,1]]

cfCube :: [(Rational, Rational, Rational)]
cfCube = [ (0,0,0) , (1,0,0) , (-1,0,0) , (0,1,0) , (0,-1,0) , (0,0,1) , (0,0,-1) ]

defaultTiptopSigN2 :: GNYTiptopConfig
defaultTiptopSigN2 = GNYTiptopConfig
  { nmax = 10
  , affine = AffineTransform
    { affineShift  = toV (1.0685647, 0.65024201, 1.7281168 , 3.4)
    , affineLinear = toV ( toV (0.0001255, 0, 0, 0)
                         , toV (0, 0.00121849, 0, 0)
                         , toV (0, 0, 0.00670419, 0)
                         , toV (0, 0, 0, 0.5)
                         )
    }
  , initialLambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
  , initialCheckpoint = Nothing
  , initialDisallowed = [] -- see: ~/BACKUP
  , initialAllowed = []  
  , initialSearch = toV <$>
    concat [
      [ (1.0686075,0.64943193,1.7237979,z)
      , (1.068561,0.64966843,1.7245763,z)
      , (1.0685582,0.64972756,1.7242114,z)
      , (1.0685963,0.64963462,1.7242366,z)
      ]
    | z <- [3.2, 3.3, 3.4, 3.5] ]
  , opeEllipse = GNYData.gny2Sig25bilinearMatNmax10
  , solverPrecision = 960
  , tiptopConfig = defaultTiptopConfig 16 250
  , jobTime = 16*hour
  , jobType = MPIJob 1 64
  , jobMemory = "128G"
  , jobPartition = "shared"
  }

defaultTiptopSigN4 :: GNYTiptopConfig
defaultTiptopSigN4 = GNYTiptopConfig
  { nmax = 10
  , affine = AffineTransform
    { affineShift  = toV (1.0435057, 0.75781443, 1.9011495 ,3.4)
    , affineLinear = toV ( toV (0.000232457, 0, 0, 0)
                         , toV (0, 0.00312404, 0, 0)
                         , toV (0, 0, 0.0188271, 0)
                         , toV (0, 0, 0, 0.5)
                         )
    }
  , initialLambda = --toV (-0.258492, -0.123751, -0.515709, -0.807397)
      toV (-0.2148, -0.07637, -0.49035, -0.84117)
  , initialCheckpoint = Nothing
  , initialDisallowed = []
  , initialAllowed = []
  , initialSearch = toV <$>
    concat [
      [ (1.0434482,0.75775924,1.9002326,z)
      , (1.0433748,0.75871336,1.9059818,z)
      , (1.0434727,0.75871336,1.9031072,z)
      , (1.043436,0.75799777,1.8987953, z)
      ]
    | z <- [3.35, 3.45, 3.6] ]
  , opeEllipse = N4.nmax6opeEllipse2
  , solverPrecision = 768
  , tiptopConfig = defaultTiptopConfig 16 250
  , jobTime = 16*hour
  , jobType = MPIJob 1 64
  , jobMemory = "128G"
  , jobPartition = "shared"
  }

defaultTiptopSigN8 :: GNYTiptopConfig
defaultTiptopSigN8 = GNYTiptopConfig
  { nmax = 10
  , affine = AffineTransform
    { affineShift  = toV (1.0211647, 0.86724505, 2.0082107 ,3.4)
    , affineLinear = toV ( toV (0.0000851138, 0, 0, 0)
                         , toV (0, 0.00234423, 0, 0)
                         , toV (0, 0, 0.020893, 0)
                         , toV (0, 0, 0, 0.5)
                         )
    }
  , initialLambda = toV (0.1661, 0.04041, 0.5, 0.855)
  , initialCheckpoint = Nothing
  , initialDisallowed = []
  , initialAllowed = []
  , initialSearch = toV <$>
    concat[
      [ (1.0211701,0.86736824,2.0087062,z)
      , (1.0211577,0.86735996,2.0087066,z)
      , (1.0211711,0.86750942,2.0114326,z)
      , (1.0211577,0.86735996,2.0087066,z)
      ]
    | z <-  [3.4, 3.6, 3.7, 3.8 ] ]
  , opeEllipse = N8.nmax6DelaunayOpeEllipse
  , solverPrecision = 768
  , tiptopConfig = defaultTiptopConfig 16 250
  , jobTime = 16*hour
  , jobType = MPIJob 1 64
  , jobMemory = "128G"
  , jobPartition = "shared"
  }

defaultTiptopSigLowerN8 :: GNYTiptopConfig
defaultTiptopSigLowerN8 = defaultTiptopSigN8
  { affine = AffineTransform
    { affineShift  = toV (1.0211647, 0.86724505, 2.0082107 ,3.4)
    , affineLinear = toV ( toV (0.0001, 0, 0, 0)
                         , toV (0, 0.0025, 0, 0)
                         , toV (0, 0, 0.025, 0)
                         , toV (0, 0, 0, -0.5)
                         )
    }
  , initialAllowed = toV <$> [(102113323/100000000,43381701/50000000,100571411/50000000,13/4),(102113323/100000000,43381701/50000000,100571411/50000000,67/20),(102113577/100000000,21679649/25000000,201118729/100000000,67/20),(102114403/100000000,43381891/50000000,201142811/100000000,13/4),(102114403/100000000,43381891/50000000,201142811/100000000,67/20),(20422893/20000000,43392621/50000000,100570407/50000000,13/4),(20422893/20000000,43392621/50000000,100570407/50000000,67/20),(51057437/50000000,43372433/50000000,50142211/25000000,67/20),(12764427/12500000,43355101/50000000,40114259/20000000,13/4),(102115583/100000000,2169707/2500000,201428021/100000000,13/4),(12764427/12500000,43355101/50000000,40114259/20000000,67/20),(102115583/100000000,2169707/2500000,201428021/100000000,67/20),(102115991/100000000,338859/390625,4017121/2000000,67/20),(102115991/100000000,338859/390625,4017121/2000000,13/4),(25529277/25000000,43375471/50000000,201143257/100000000,67/20),(25529277/25000000,43375471/50000000,201143257/100000000,13/4),(51058733/50000000,86701979/100000000,200312159/100000000,67/20),(51058733/50000000,86701979/100000000,200312159/100000000,13/4),(25529379/25000000,43355283/50000000,100285643/50000000,13/4),(102117759/100000000,17344013/20000000,201118683/100000000,13/4),(25529379/25000000,43355283/50000000,100285643/50000000,67/20),(102117759/100000000,17344013/20000000,201118683/100000000,67/20),(10784483889966208973/10560575538856030000,532505779073003969/614583427775606000,835454397445287749/417355909087189500,13/4),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,870737511/268435456),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,432852173/134217728),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,869059789/268435456),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,217894093/67108864),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,435997901/134217728),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,872205517/268435456),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,872310375/268435456),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,218090701/67108864),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,872402125/268435456),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,436194509/134217728),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,218102989/67108864),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,872408679/268435456),(960463800330831619/940555689972540000,3775467643656328583/4354611903946342000,26885086313194831/13400960809727600,432852173/134217728),(22796760974273667809/22324270916297890000,143041073270350309/164938288055510000,1129380921777443189/562949953421312000,432852173/134217728),(18816434387269102489/18426192357592790000,592309773583924741/683161608698574000,159966777734236513/79735987239164625,432852173/134217728),(2570029130188088401/2516766937388310000,188787453925332571/217625373072830000,50587393513206641/25152475239201500,432852173/134217728),(5961552072156780101/5838073250186410000,2843327767339696751/3277686953079134000,175353381210187201/87187152628035000,432852173/134217728),(1277633892885637949/1251145228623970000,1012232107596658127/1167332439168698000,1186932177968722607/590887293623711000,432852173/134217728),(7257365364120955069/7106911578314190000,1901817593408385013/2192629956758002000,268303394091738181/133570252269716750,432852173/134217728),(290835403954683321/284811876740780000,1068532579834715623/1231431897822922000,436949847345216589/217257495338623250,432852173/134217728),(3066533303767213561/3002946157581560000,975373870908486377/1125454561370598000,633969738120195631/316398709807650500,432852173/134217728)]
  , initialDisallowed = toV <$> [(102109669/100000000,5424937/6250000,8077939/4000000,63/20),(102109669/100000000,5424937/6250000,8077939/4000000,13/4),(102109669/100000000,5424937/6250000,8077939/4000000,67/20),(816903/800000,17348177/20000000,8035367/4000000,63/20),(816903/800000,17348177/20000000,8035367/4000000,67/20),(816903/800000,17348177/20000000,8035367/4000000,13/4),(5105647/5000000,4341129/5000000,100712789/50000000,63/20),(5105647/5000000,4341129/5000000,100712789/50000000,67/20),(5105647/5000000,4341129/5000000,100712789/50000000,13/4),(102113323/100000000,43381701/50000000,100571411/50000000,63/20),(25528337/25000000,21695551/25000000,25106701/12500000,63/20),(25528337/25000000,21695551/25000000,25106701/12500000,13/4),(102113577/100000000,21679649/25000000,201118729/100000000,63/20),(25528337/25000000,21695551/25000000,25106701/12500000,67/20),(102113577/100000000,21679649/25000000,201118729/100000000,13/4),(102114403/100000000,43381891/50000000,201142811/100000000,63/20),(20422893/20000000,43392621/50000000,100570407/50000000,63/20),(51057437/50000000,43372433/50000000,50142211/25000000,63/20),(51057437/50000000,43372433/50000000,50142211/25000000,13/4),(638219/625000,17364589/20000000,201425569/100000000,63/20),(638219/625000,17364589/20000000,201425569/100000000,13/4),(102115283/100000000,21708111/25000000,100986483/50000000,63/20),(102115283/100000000,21708111/25000000,100986483/50000000,13/4),(638219/625000,17364589/20000000,201425569/100000000,67/20),(102115283/100000000,21708111/25000000,100986483/50000000,67/20),(12764427/12500000,43355101/50000000,40114259/20000000,63/20),(102115583/100000000,2169707/2500000,201428021/100000000,63/20),(102115991/100000000,338859/390625,4017121/2000000,63/20),(25529277/25000000,43375471/50000000,201143257/100000000,63/20),(51058733/50000000,86701979/100000000,200312159/100000000,63/20),(25529379/25000000,43355283/50000000,100285643/50000000,63/20),(102117759/100000000,17344013/20000000,201118683/100000000,63/20),(2330459550011985601/2282301716294660000,279190364428003709/321723658012726000,28034817692147953/14021340200770625,13/4),(13038214124182947811/12767708139007010000,2527294394091915429/2912211946309186000,1442222012040641111/720513381984758000,13/4),(2001051361707454867/1959557349116970000,113042999576930839/130292702629138000,136982877421134853/68433093239005250,13/4),(5326339451492052833/5215934198738530000,142937601071410047/164407177247854000,395743652498758763/195281040177520250,13/4),(2454958568787103733/2404010175185680000,2656823014527255087/3061482186614918000,298179230197016327/149075722760282250,13/4),(368849826686244173/361220568575590000,2589343202811244723/2986351883227022000,101824406791109351/50997602719467500,13/4),(1558246001485464593/1525927724530280000,1384816156527575327/1595973717949878000,979299060787837433/483851020960186500,13/4),(1254521063457701473/1228639828489190000,61165849905875381/70368744177664000,569295643492360377/281474976710656000,13/4),(2991586267183591229/2929758670285890000,64363442658061927/74168596750271750,2046146061428364611/1022977816547630500,13/4),(14268560405387353019/13972813137809590000,1597438176293260379/1837672012468246000,426938088473987581/210790246058801750,13/4),(2024435302659369029/1982585375259590000,5344481115606336943/6158611696284122000,581405148357675373/290460411272521500,13/4),(5627454813307897241/5510809827103660000,2387020589018124851/2745705096293934000,570312761059314387/281474976710656000,13/4),(12336400669154149759/12080670054916790000,1211634589914508639/1396407839472286000,54026505469144393/26732254699529800,13/4),(54145861609087455359/53021991077140090000,2819469599395500893/3248908842863442000,187674030314072839/93863321454602625,13/4),(3280124783540979373/3212471282254715000,1224620331704405127/1408726900412678000,137540800848429703/67978934148909625,13/4),(10232470036715462947/10020574740083570000,1100959010123039263/1271345195627062000,692391712827349127/346764571537353500,13/4),(11784781126315207751/11540530586135710000,2569861889104657609/2956955960124186000,2200391008053367907/1087173663565766000,13/4),(18217598925325858129/17840182758179840000,1245843766663673867/1433099964197798000,1400888892788717631/692383072718345500,13/4),(4986110293535965463/4883194674596980000,1005568514765758707/1157104521355658000,1109955839086597489/549190972705947000,13/4),(12729457026287005817/12465747536575570000,4218869740370466333/4853502266830882000,111892708658489247/55322738481403250,13/4),(3409146354022041157/3338564533534870000,78073634104672643/89868265655149500,267981268189148773/132636331439311500,13/4),(2472423160583819133/2421234189600130000,489091043648519123/562949953421312000,831345716639772321/411400034652589000,13/4),(306334760298323489/299997289868506000,507647653999647269/585852112268006000,694047284035362759/346601628959587000,13/4),(982569241423549379/962215137605710000,1661172817575266171/1911466507721574000,1136951345885137259/562552748837454500,13/4),(581855898877473551/569844094875205000,1605453907530240547/1850310076458998000,496009718438796049/245067976245860750,13/4),(5447288622782133899/5334132743961340000,3283910157908907103/3783293025502582000,456056911722083627/226418207176908500,13/4),(6014609955475713553/5889220509338630000,1942576971424803379/2245954004608726000,466410382852000807/234455415343742250,13/4),(944584126834581971/925012947330970000,623536369317754381/718889962828626000,54200279810791109/26982676795782375,858993459/268435456),(2653143446539880871/2597941132935910000,177922773606463511/205332741271150000,220967718694455747/109698462921767000,13/4),(19299777429854779831/18898508551353110000,1011129902987659803/1167146606197262000,621342581402676637/310484520330556000,432852173/134217728),(5832414880731257653/5711694245886730000,1342440512730735009/1545183201847306000,1136432097378646419/562949953421312000,432852173/134217728),(205982746916962913/201705691622602000,804881305985750907/929482132300298000,37726362760822037/18875417609297000,432852173/134217728),(456190967050989509/446724477372802000,1181310790765063339/1361143111028786000,35515050818870347/17592186044416000,432852173/134217728),(242150910951313281/237111403145450000,2175776102798895691/2512504166854414000,2190796093688242379/1096109393102567000,432852173/134217728),(14188210270079368093/13893684160006430000,1537020191686847611/1773031586075254000,582352290804192773/291378059970927750,432852173/134217728)]
  , initialSearch = let z=3.21 in toV <$> [(1.02116,0.867245,2.00821,z),(1.02116,0.867245,2.0108,z),(1.02116,0.867245,2.00562,z),(1.02116,0.867551,2.00821,z),(1.02116,0.866939,2.00821,z),(1.02117,0.867245,2.00821,z),(1.02115,0.867245,2.00821,z)]
  , tiptopConfig = defaultTiptopConfig 12 250
  }

defaultTiptopEpsLowerN8 :: GNYTiptopConfig
defaultTiptopEpsLowerN8 = defaultTiptopSigN8
  { affine = AffineTransform
    { affineShift  = toV (1.0211647, 0.86724505, 2.0082107 ,3.4)
    , affineLinear = toV ( toV (0.0001, 0, 0, 0)
                         , toV (0, 0.0025, 0, 0)
                         , toV (0, 0, 0.025, 0)
                         , toV (0, 0, 0, -0.5)
                         )
    }
  , initialAllowed = toV <$> [(2788307943937173941/2730660661232650000,62768774323952669/72385956480242000,453710212696051461/226466986884969250,1085351781/335544320),(2131058202319494049/2086996063002990000,2461587408699127221/2839124938817194000,1138244390491841569/568016342373869500,1085351781/335544320),(10053626155275343267/9846022013080595000,769247738521889531/886602230917194000,188107111894693181/93854612067006125,1085351781/335544320),(11566232470297239599/11327093711512990000,104350606260315341/120357362417502000,181709185297005103/90711956209279625,1085351781/335544320),(1889152609665626407/1850096840288495000,433978230511210507/500393440390178000,1175626336437961009/586724681028792000,1085351781/335544320),(509170944199035279/498654610748815000,1292053383333148157/1489785771124538000,196321490697632273/97984130715548000,1085351781/335544320),(1570508663677435747/1537973589184020000,6682400016370273289/7708640263673266000,679119829154443581/339039001328430500,1085351781/335544320),(2523008212639638849/2470829882959490000,1765455175994452057/2035728968690538000,950813987795860847/475036885919406000,1085351781/335544320),(3578716857531101933/3504630284028630000,376717381024845319/434397481076454000,1121523529727935087/559537868064351000,1085351781/335544320),(14088583745147101003/13797982307010680000,1232350617535365721/1420162838976674000,434738806337912573/216707489007111500,1085351781/335544320),(2294864804329608317/2247292684517845000,2342374038599808291/2701484950068374000,461019036983615398/230468181752842125,1085351781/335544320),(3420414310397711621/3349708396801660000,1761055560771332671/2030264810364134000,335491366617445897/167474836062781000,1062769587/335544320),(3420414310397711621/3349708396801660000,1761055560771332671/2030264810364134000,335491366617445897/167474836062781000,268515171/83886080),(22229895754320789721/21770743185862910000,1778964742431632973/2051017821345362000,154115214157067647/76891694543122000,1062769587/335544320),(4080549910668520003/3996226790504230000,501558232195795971/578139714078284000,169743662244659/84712644341454,1062769587/335544320),(5388224342339563553/5276916813596230000,632478213755413431/728935048946474000,698938156178412101/348722451009710500,1062769587/335544320),(4337432590872530369/4247614958390690000,816704354177305341/941830174359574000,1215101617459902491/606886586316910500,1062769587/335544320),(6508657776716258581/6374082387251510000,1467908491230813323/1692360063362542000,685465689083875967/341994556601246000,1062769587/335544320),(552085251503785273/540664856954450000,170548361574427007/196681535717748000,918038539509145599/458517904459209500,1062769587/335544320),(3355373976420996709/3286028082998840000,1797114920917186953/2071768966584922000,811790160826270601/405458923439513000,1062769587/335544320),(50702240169273951/49655718243332000,1370126594977054803/1579161020218102000,910239931931434371/453902808736498000,1062769587/335544320),(16500177081564745019/16159130423146990000,15261394358759259/17592186044416000,2956628403674862827/1474744335477368500,1062769587/335544320),(2359560660800584237/2310891516740270000,1072233694440290757/1235431977843578000,8335131112090192/4154249810439575,1062769587/335544320),(21947494704764866837/21492385319030870000,461278120391139861/532023104876126000,586656065296560341/293083716245838000,1062769587/335544320),(1196051416715177303/1171301080665220000,1834475374624354529/2114867719298186000,116437936859853203/58124906682701500,1062769587/335544320),(22377727397789359877/21914664664014970000,1803177869504414881/2079806009311534000,554203399908720887/276871467304379750,1062769587/335544320),(721521960317955923/706625793020750000,38360574756371701/44224088052298000,313623743941729553/156643529661624000,1062769587/335544320),(3252122560698740447/3184764003090370000,1926572895933629827/2221812142135838000,326163144200876601/162815205619769875,1062769587/335544320),(109259607706731692/107001560619510625,1778850271944209297/2050269545928738000,819674261996920493/408520023847926500,1062769587/335544320),(11325312352056460439/11091684458939140000,569442715084086889/656195789727166000,709666060958817621/353982797617550500,1062769587/335544320),(5347838475184609673/5237162361264580000,1256153355505255891/1449042054515374000,953623153116789041/476798167019163000,1062769587/335544320)]
  , initialDisallowed = toV <$> [(1086280667053703861/1063781820237635000,831770367495984737/958646886148402000,6975387527023074761/3477090815666793000,1085351781/335544320),(1498777162974760483/1467974783002430000,6637657690733870819/7639851607239046000,31529723079181916/15720464902447125,1085351781/335544320),(8929639719970068437/8747849329402270000,4968221611046244341/5712961621452674000,982638732558753427/489523241317806000,1085351781/335544320),(3344834059850532761/3275124039919060000,156639877736788253/180863869216372000,39591862584845719/19776069050391250,1085351781/335544320),(607893012468935479/595239027035405000,1466410688342879189/1692219573401666000,557935528045367549/278574901346416000,1085351781/335544320),(1320233169645798907/1292754144492132500,203126588025032419/234868134631414000,840081864607587319/419873805955299500,1085351781/335544320),(1685913723654047067/1651355462204170000,1304565488752822157/1499785574244438000,815212636068614533/406118436821414000,1085351781/335544320),(10723087034835836123/10500751283900530000,58236187130772063/67139414850076375,59466133020330427/29726656120051700,1085351781/335544320),(8365277148332631139/8193929835683990000,30569515196602859/35189516905751625,1000127926111960781/498653199936495500,1085351781/335544320),(6307334853066906949/6176349105703040000,250468394099789821/289428418994968400,368701000122313889/184261118251700750,1085351781/335544320),(5413719784044863809/5301706484948390000,1347863291235070401/1552561715302754000,1131157788025712979/562949953421312000,1085351781/335544320),(1185264475314563747/1161116633000120000,6893183165112834791/7928283514695294000,223941442567228643/111482493605586000,1085351781/335544320),(2902945596940673819/2843174154100890000,650125448025296121/749965981961674000,13040451007300619/6519006720788375,1085351781/335544320),(4369558411685764507/4278933282964570000,1586099901301248317/1831802935080178000,1622803624756215497/809994840604281000,1085351781/335544320),(5837700459527113741/5715521040182210000,1408686673887766403/1627910450609982000,1203938817084897641/601595294574018000,1085351781/335544320),(3769798694024121273/3692326546319830000,1534214587585125899/1767612663158126000,252348971995178011/125587048169552375,1085351781/335544320),(2240244548886447431/2194068015867160000,1180238066056599853/1361883483245802000,314771968604672403/157333852609712750,1085351781/335544320),(7226867101397530717/7077870099809270000,1489700194274922981/1718553983635234000,1311817105459381237/656457620090218500,1085351781/335544320),(3201547107381586657/3134525303909945000,1409701284392197217/1628684682867218000,8245612248072423/4125057927272050,1085351781/335544320),(866825181658953931/848847162493700000,1616833986265564561/1863947488364794000,856729205689082403/426844017259866500,1085351781/335544320),(838646240648069173/821379684422470000,1198849530909693771/1379410193804474000,412651453228416184/205605135148791375,1085351781/335544320),(4015036250502104783/3931308832606130000,1454153233554637543/1683080153222902000,227742973371016969/113823724707948250,1085351781/335544320),(484612541888601989/474525159350550000,188689683853007869/218054340055442000,1164969796293026913/581978614368071500,1085351781/335544320),(56138508332109359/54973242904827500,76707738222736561/88471906381924000,327083147490843603/163679720734596500,1085351781/335544320),(3183838098115836033/3116388726245930000,766821761056386747/887495755583638000,299233205581856534/149596747881751375,1085351781/335544320),(34330807500967038/33622285135675625,631008856650755661/727961048901224000,2503183592231878811/1250876530629713000,1085351781/335544320),(1801216330146750437/1764471708758110000,1597945435898412251/1833509780318414000,1729645903474360439/859930623806399500,1085351781/335544320),(4261582192371516157/4174393854639620000,2047832307529433151/2358693261452414000,70835533966385989/35184372088832000,1085351781/335544320),(208702890800122109/204371360490707500,263478553559628707/303352487343138000,208411054817489822/103554638843065375,1085351781/335544320),(3704740851202475817/3628732848162770000,25478050267789057/29290144737505680,443529201974843263/220954183443636500,1085351781/335544320),(11722723384308612629/11476798011049990000,354703876557525499/409943768775666000,669106964608388883/334813940876606500,1085351781/335544320),(7492019081772902687/7337781747450670000,653507520926677449/753527071312466000,1200485107655480361/597937960664780500,1085351781/335544320),(4435754087081659811/4344471874023010000,2042476416529469497/2356332992433218000,289376026233253391/144701898034071000,1085351781/335544320),(4922319848770988491/4820236054853510000,1650840080211656187/1903217917710418000,1403451643529455141/701576851574248000,1085351781/335544320),(12723439581506253037/12464155293978770000,766480831655572129/882018065905346000,949513818324626491/473099215600838000,1085351781/335544320),(6637551781719931451/6500779036586110000,1832731317740010607/2114454393193638000,528718526618010783/264655498334445250,1085351781/335544320),(2640607977316363561/2586293983811360000,1141717504281541227/1313174688728038000,118676361893043937/59117351602704125,1085351781/335544320),(9744061783931987669/9539441349376690000,901667952915395413/1042109576453162000,25351904207281679/12667967476859850,1085351781/335544320),(10540102976099317673/10321182138855130000,3515636899951208837/4054975671469978000,1040107326449018409/520641077734394500,1085351781/335544320),(3214845696673290763/3148050606280330000,837185347527226629/964844049533726000,450940166569576553/224552612417817750,1085351781/335544320),(2777674539756958189/2719923169473390000,49683495046745569/57306615391410500,1527872938876172731/763722651118963000,1085351781/335544320),(16812708053738151923/16465762365394330000,1085953369007206969/1251591900281946000,204262921810585459/101913365535223000,1062769587/335544320),(6056339552147059937/5931041486993770000,1242224308371049837/1432046729864938000,820986878275520199/410051919983557000,1062769587/335544320),(2440111003833635177/2389721590940350000,2131418247030290273/2456087156987042000,170911905688068154/85251100227158875,1062769587/335544320),(810893484824384683/794121688515770000,2227269985457812547/2569495340349198000,786947319886912439/393037327378482000,1062769587/335544320),(15403275485391641317/15084982411399370000,61030846385054671/70368744177664000,437955431693278231/218270344784375500,1062769587/335544320),(1485038923772274871/1454364860893790000,441743472844994341/509158823986234000,239071601896589683/119440716676997500,1062769587/335544320),(3548601393786457373/3475340995648630000,15690926742790714759/18081505088349846000,975040994640934173/484915796698964000,1062769587/335544320),(9252262827997236863/9060661702448530000,122133245434081897/140737488355328000,365852141307592883/182831084444046500,1062769587/335544320),(11529440773943041427/11293282672409170000,851394995086568497/979825533715578000,8492681255626473939/4223968819076422000,1062769587/335544320),(6513522372355465907/6379541797203670000,232943847959424879/268062297480848000,500747923442411089/249053850189699500,1062769587/335544320),(4401818972794571311/4310765977840210000,3643183254506439199/4199612283461486000,565984373468383337/281474976710656000,1062769587/335544320),(4419443457862469117/4328368226144870000,1712700472086214277/1973123264570738000,525770570438948799/262612375118492000,1062769587/335544320),(4801531235075234107/4702819800653720000,322104631358114971/371207009231038000,763786008967104207/379854198524471000,1062769587/335544320),(6725881928110604117/6587336987595970000,1228757949166310731/1416536427081634000,412561535301946279/205175670618989500,1062769587/335544320),(7902833925116692849/7739289648293090000,1154060689202664497/1329438474550938000,445412929545749041/222474352001788000,1062769587/335544320),(2312152898101578409/2264460182142690000,886701156756492319/1022102465032918000,630457517456590473/315573186109361500,1062769587/335544320),(1370989133316021071/1342785047154710000,668296497936397817/769143932160998000,1579717523165536153/786322605992229000,1062769587/335544320),(55841092475807226329/54691395425208190000,58332478830936803/67164379755372400,383734706266614647/191261082368291000,1062769587/335544320),(46096847745610920059/45142113895625590000,23715083714800469/27376169440500000,641741690498464239/320850770317346500,1062769587/335544320),(5747332355703263797/5629499534213120000,2611748941332621079/3007413826376526000,9658961153561693/4814239904287700,1062769587/335544320),(2277787229831601587/2230697989676070000,493116227580839237/568601523087448000,1130768142203174359/562949953421312000,1062769587/335544320),(190129589161713049/186217207385888000,385378415780789383/444376443775962000,330952286941264479/164942276234088250,1062769587/335544320),(6932850382854190001/6789246768447210000,1206502292733530387/1390631584776858000,165049278528534211/82614467888163000,1062769587/335544320),(958739586158560471/938799727466472500,84033293967105861/96998749239788000,546762111349741721/273362828141605500,1062769587/335544320),(7438963755248538661/7286290121674010000,1153410711619117029/1327833591864206000,70810444792254237/35218032957397250,1062769587/335544320),(2377448230311030681/2328504410437810000,1652239305136511049/1905306766971626000,1000016438140056951/497857598458718000,1062769587/335544320),(736984485817447457/721775906838810000,476691338982008161/549519022631384000,931370631508320341/465689215936488000,1062769587/335544320),(392777511041772273/384681375560210000,364420206608687683/419549203383446000,1024134889329422167/509765423840036000,1062769587/335544320),(2561232091683885977/2508444955279150000,1269680109141989593/1461573992396882000,688789384552903083/342572456913371500,1062769587/335544320),(929032951212061411/909780091265270000,1038192922563769391/1198106408412414000,411298763681877753/205417482136110250,1062769587/335544320),(1521987441165925339/1490425543511140000,682920525214649347/787056531139078000,132427089648097283/66196422410254000,1062769587/335544320),(4122759692647886957/4037523640874095000,170546130672700729/196684204443446000,907613982383986559/454413436676753875,1062769587/335544320),(11918268953570341193/11670967743982930000,752840688565334919/869427749619406000,692956148239304811/346916446339973000,1062769587/335544320),(1431065633554249477/1401247568271710000,617677287496184389/713277888456846000,893792409870961037/447459786666891000,1062769587/335544320),(2729442327486017431/2672911419405335000,563747397148520131/650317535151834000,272939598948643063/136061930843873375,1062769587/335544320),(87456459312158091/85641543141694375,70250844916193257/81140205111918000,141320464633829873/70806593177347500,1062769587/335544320),(5409607489975938387/5297298441370370000,1266759725178927491/1460792803787734000,897414116810594957/449304838989708500,1062769587/335544320),(8055974875173095887/7888098567412570000,178934741080047317/206655005475602000,474516815775032011/237748869591195500,1062769587/335544320),(2952066819740045939/2891026991512890000,6991297611443104777/8063794706748178000,593663913654445241/297467941558170500,1062769587/335544320),(3415148414360824567/3344237976591770000,1701510366376205707/1962379637391238000,334133249496864469/167423887753758250,1062769587/335544320),(707685166467918191/693087418586870000,1290320081119479479/1488763455106126000,1339531842322052717/668302251423688500,1062769587/335544320),(2169099823146161007/2124132894358270000,1042445297961437577/1202352500796818000,141272589635069733/70368744177664000,1062769587/335544320),(3792464535604676971/3713656378487710000,87902655489519809/101352393954758000,519059371770193507/259666270942453500,1062769587/335544320),(1333971101573268181/1306118432144990000,733940607624476809/847503312333916000,1124484134821248039/562949953421312000,1062769587/335544320),(384664675374326411/376758971415372500,1222350144261301943/1408385767395802000,1347278611779409333/672214756111374000,1062769587/335544320),(4985060304160718789/4881525980182540000,1456039088001923219/1681283687863386000,442838955461600833/221521193728380250,1062769587/335544320),(13470532744715747093/13192292986041130000,337401817026069737/388618898883228000,294753275077855959/146828792827773250,1062769587/335544320)]
  , initialSearch = toV <$> concat [
      [(16500177081564745019/16159130423146990000,15261394358759259/17592186044416000,2956628403674862827/1474744335477368500,z),(3252122560698740447/3184764003090370000,1926572895933629827/2221812142135838000,326163144200876601/162815205619769875,z),(5347838475184609673/5237162361264580000,1256153355505255891/1449042054515374000,953623153116789041/476798167019163000,z),(5347838475184609673/5237162361264580000,1256153355505255891/1449042054515374000,953623153116789041/476798167019163000,z)]
      | z <- [3.04, 3.08, 3.12]]
  , tiptopConfig = defaultTiptopConfig 15 300
  }

------------ END TIPTOP ------------------------
------------ START SPECTRUM --------------------
remoteComputeWithSpectrum :: Int -> (Int -> GNYSPE.GNYSigPsiEps) -> Cluster SDPB.Output
remoteComputeWithSpectrum nmax boundFromNmax = do
  -- Log.info "cluster started" "a"
  -- Log.info "boundKey" (boundFromNmax nmax)
  -- Log.info "bound" bound
  wkDir <- newWorkDir bound
  -- Log.info "precision" $ SDPB.precision (solverParams bound)
  Log.info "point workdir" wkDir
  let sdpParts = Bound.withSDPDeps bound SDPB.parts
      conFiles = SDPB.partPath "" <$> drop 2 sdpParts
      bulkCons = GNYSPE.bulkConstraints (boundKey bound)
      -- see Bounds file for constraint ordering
      constraintStrs = zipWith ($)
        (map prettyPrintCh bulkCons ++ map prettyPrintNamedCh ["external", "current"])
        conFiles
  -- dump channels
  liftIO $ writeFile (wkDir </> "spectrumKey.json") ("[" ++ intercalate ",\n" constraintStrs ++ "]\n")
  -- run
  Bound.remoteComputeWithFileTreatment Bound.keepAllFiles Bound.defaultBoundFiles bound
  where
    bound = Bound
      { boundKey = boundFromNmax nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax)
        { precision     = 768
        , writeSolution = Set.fromList [SDPB.PrimalVector_x, SDPB.DualVector_y]
        , procGranularity = 8
        }
      , boundConfig = defaultBoundConfig
      }
    prettyPrintCh (GNYSPE.BulkConstraint _ bulkChannel) fname =
      --Q3M.GeneralChannel cRep fRep
      "{" ++ 
      withTag "filename" fname ++ ", " ++
      withTag "rep" ("rep[" ++ flavorRepName ++ "]") ++ ", " ++
      withTag "l" (show $ B3d.spin cRep) ++ ", " ++
      (case B3d.delta cRep of
         (Blocks.Fixed dim) -> withTag "bound" (fromRational dim)
         _ -> withTag "bound" "unitarity"
      ) ++ "}"
      where
        (flavorRepName, cRep) = repOfCh bulkChannel
    prettyPrintNamedCh chName fname =
      "{" ++ 
      withTag "filename" fname ++ ", " ++
      withTag "namedChannel" chName ++ "}"
    withTag :: (Show a) => String -> a -> String
    withTag tagName value = show tagName ++ ": " ++ show value
    repOfCh :: GNYSPE.Channel j B3d.Block3d -> (String, B3d.ConformalRep Blocks.Delta)
    repOfCh (GNYSPE.Scalar_ParityEven_Singlet        delta) = ("Scalar_ParityEven_Singlet"
                                                            , B3d.ConformalRep delta 0)
    repOfCh (GNYSPE.Scalar_ParityEven_TensorSym      delta) = ("Scalar_ParityEven_TensorSym"
                                                            , B3d.ConformalRep delta 0)
    repOfCh (GNYSPE.SpinEven_ParityEven_Singlet      cRep ) = ("SpinEven_ParityEven_Singlet"     ,cRep)
    repOfCh (GNYSPE.SpinEven_ParityOdd_Singlet       cRep ) = ("SpinEven_ParityOdd_Singlet"      ,cRep)
    repOfCh (GNYSPE.SpinOdd_ParityOdd_Singlet        cRep ) = ("SpinOdd_ParityOdd_Singlet"       ,cRep)
    repOfCh (GNYSPE.SpinEven_ParityEven_TensorSym    cRep ) = ("SpinEven_ParityEven_TensorSym"   ,cRep)
    repOfCh (GNYSPE.SpinEven_ParityOdd_TensorSym     cRep ) = ("SpinEven_ParityOdd_TensorSym"    ,cRep)
    repOfCh (GNYSPE.SpinOdd_ParityOdd_TensorSym      cRep ) = ("SpinOdd_ParityOdd_TensorSym"     ,cRep)
    repOfCh (GNYSPE.SpinOdd_ParityEven_TensorAntiSym cRep ) = ("SpinOdd_ParityEven_TensorAntiSym",cRep)
    repOfCh (GNYSPE.SpinOdd_ParityOdd_TensorAntiSym  cRep ) = ("SpinOdd_ParityOdd_TensorAntiSym" ,cRep)
    repOfCh (GNYSPE.SpinEven_ParityOdd_TensorAntiSym cRep ) = ("SpinEven_ParityOdd_TensorAntiSym",cRep)
    repOfCh (GNYSPE.ParityEven_Vector                cRep ) = ("ParityEven_Vector"               ,cRep)
    repOfCh (GNYSPE.ParityOdd_Vector                 cRep ) = ("ParityOdd_Vector"                ,cRep)


minCT :: V 3 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSPE.GNYSigPsiEps
minCT deltaExts mbLambda ngroup nmax =
  basicBound { GNYSPE.objective = GNYSPE.StressTensorOPEBound UpperBound }
  where
    basicBound = GNY2020.gnySigPsiEpsFeasibleDefaultGaps deltaExts mbLambda ngroup nmax

------------ END SPECTRUM ----------------------

navigatorPoints :: DB.KeyValMap (V 4 Rational) SDPB.Output
navigatorPoints = DB.KeyValMap "navigatorPoints"

gnyNavigatorGrid :: Rational -> Int -> Maybe (V 4 Rational) -> [[V 4 Rational]] -> Cluster ()
gnyNavigatorGrid nGroup nmax mLambda pointGroups =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory .
         setSlurmPartition jobPartition) $
  forM_ pointGroups $
  \points -> forConcurrently_ points $
             \point -> do
               output <- Bound.remoteCompute (bound point)
               DB.insert navigatorPoints point output
               Log.info "Finished point" (point, output)
  where
    jobTime = 12*hour
    jobType = MPIJob 1 64
    jobMemory = "128G"
    jobPartition = "shared"
    prec = 768
    bound v = Bound
      { boundKey = --(gnyFeasibleSPP sDims deltaExts mLambda nGroup nmax)
          (gnyFeasibleEPP sDims deltaExts mLambda nGroup nmax)
          { GNYSPE.objective = GNYSPE.GFFNavigator mLambda }
      , precision = prec
      , solverParams = (Params.optimizationParams nmax)
                       { precision = prec
                       , procGranularity = 4 }
      , boundConfig = defaultBoundConfig
      }
      where
        sDims = toV (dsp, 4.5)
        deltaExts = toV (x,y,z)
        (x,y,z,dsp) = fromV v
  

----------------------------------------------------------------
boundsProgram :: Text -> Cluster ()

boundsProgram "gnySkydiving_6d_nmax6" =
  local (setJobType (MPIJob 1 16) . setJobTime (12*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpGNY3d 6 2 2.99 5.0)
  Log.info "Skydiving result" result

boundsProgram "gnySkydiving_6d_grid_nmax6" =
  local (setJobType (MPIJob 1 64) . setJobTime (24*hour) . setJobMemory "100G") $
  mapConcurrently_ (\(ngroup,sp,spp) -> remoteSearch ngroup sp spp
                                        >>= uncurry (prettyPrintResult (ngroup,sp,spp)))
  trialPoints
  where
    remoteSearch ngroup sp spp = remoteEvalJob $
      cPtr (static testDynamicalSdpGNY3d)
      `cAp` cPure nmax `cAp` cPure ngroup `cAp` cPure sp `cAp` cPure spp
    nmax = 6
    trialPoints =
      [ (2,sp,spp) | sp <- [2.98, 3.02], spp <- [5.05, 5.1 , 5.15] ] ++
      [ (4,sp,spp) | sp <- [2.98, 3.02], spp <- [3.8 , 4   , 4.2 ] ] ++
      [ (8,sp,spp) | sp <- [2.98, 3.02], spp <- [4.5 , 4.7 , 4.9 ] ]   
   --   [ (2,sp,spp) | sp <- [2.9, 2.98, 3.02], spp <- [5.05, 5.1 , 5.15, 5.2] ] ++
   --   [ (4,sp,spp) | sp <- [2.9, 2.98, 3.02], spp <- [3.8 , 4   , 4.2,  4.4] ] ++
   --   [ (8,sp,spp) | sp <- [2.9, 2.98, 3.02], spp <- [4.5 , 4.7 , 4.9 , 5.1] ]

boundsProgram "gny_newDelaunay_ngroup2_nmax10" =
  gnyDelaunaySearchOPEScan 2 defaultSearchDataN2 $ toV (1.06861, 0.65, 1.725)
  -- toV (1.06861, 0.65, 1.725)

boundsProgram "gny_newDelaunay_ngroup4_nmax10" =
  gnyDelaunaySearchOPEScan 4 defaultSearchDataN4 $ toV (1.04317845, 0.76049495, 1.90025551)
  -- toV (1.04356, 0.7578, 1.899)

boundsProgram "gny_newDelaunay_ngroup8_nmax10" =
  gnyDelaunaySearchOPEScan 8 defaultSearchDataN8 $ toV (1.02110924, 0.86665224, 1.99999196)
  -- toV (1.02119, 0.8665, 2.002)

boundsProgram "gny_newDelaunay_ngroup2_nmax8" =
  gnyDelaunaySearchOPEScan 2 (setNmax 8 defaultSearchDataN2) $ toV (1.06861, 0.65, 1.725)

boundsProgram "gny_newDelaunay_ngroup4_nmax8" =
  gnyDelaunaySearchOPEScan 4 (setNmax 8 defaultSearchDataN4) $ toV (1.04317845, 0.76049495, 1.90025551)

boundsProgram "gny_newDelaunay_ngroup8_nmax8" =
  gnyDelaunaySearchOPEScan 8 (setNmax 8 defaultSearchDataN8) $ toV (1.02110924, 0.86665224, 1.99999196)

boundsProgram "gny_newDelaunay_ngroup2_nmax6" =
  gnyDelaunaySearchOPEScan 2 (setNmax 6 defaultSearchDataN2) $ toV (1.06861, 0.65, 1.725)

boundsProgram "gny_newDelaunay_ngroup4_nmax6" =
  gnyDelaunaySearchOPEScan 4 (setNmax 6 defaultSearchDataN4) $ toV (1.04356, 0.7578, 1.899)
--  gnyDelaunaySearchOPEScan 4 searchCfg $ toV (1.04317845, 0.76049495, 1.90025551)
--  where
--    searchCfg = (setNmax 6 defaultSearchDataN4) {
--      initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/YAIdW/Object_sfcMy2t-ZamFemlURiSSj8DkBN68P6a9RsZgSS-CDME/ck"
--      }
  
boundsProgram "gny_newDelaunay_ngroup8_nmax6" =
  gnyDelaunaySearchOPEScan 8 (setNmax 6 defaultSearchDataN8) $ toV (1.02110924, 0.86665224, 1.99999196)
--  --toV (1.02119, 0.8665, 2.002)
--  where
--    searchCfg = (setNmax 6 defaultSearchDataN8) {
--      initialCheckpoint = Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2023-06/dKjBs/Object_AdaFHrTM1Wy4KxkFzMbFdq_AgpIKhdtbck423WB141I/ck"
--      }

boundsProgram "gny_cloudSearch_ngroup2_nmax8" =
  gnyCloudSearch 2 (withNewSearchPoints $ setNmax 8 defaultSearchDataN2)
  (toV (1.06861, 0.6500, 1.725)) (toV (0.00012, 0.0012, 0.007))

boundsProgram "gny_cloudSearch_ngroup4_nmax6" =
  gnyCloudSearch 4 (withNewSearchPoints $ setNmax 6 defaultSearchDataN4)
  (toV (1.04356, 0.7578, 1.899)) (toV (0.00016, 0.0015, 0.01 ))

boundsProgram "gny_cloudSearch_ngroup8_nmax6" =
  gnyCloudSearch 8 (withNewSearchPoints $ setNmax 6 defaultSearchDataN8)
  (toV (1.02119, 0.8665, 2.002)) (toV (0.00005, 0.0013, 0.012))

-- gny_tiptop_ngroup2_nmax10
boundsProgram "gny_tiptopU_ngroup2_nmax10" = gnyTiptopSearchOPEScanSig 2 defaultTiptopSigN2
boundsProgram "gny_tiptopU_ngroup4_nmax10" = gnyTiptopSearchOPEScanSig 4 defaultTiptopSigN4
boundsProgram "gny_tiptopU_ngroup8_nmax10" = gnyTiptopSearchOPEScanSig 8 defaultTiptopSigN8
boundsProgram "gny_tiptopRefineLowerS_ngroup8_nmax10" = gnyTiptopSearchOPEScanSig 8 defaultTiptopSigLowerN8
boundsProgram "gny_tiptopRefineLowerE_ngroup8_nmax10" = gnyTiptopSearchOPEScanEp  8 defaultTiptopEpsLowerN8
-- boundsProgram "gny_tiptop_ngroup8_nmax10" = gnyTiptopSearchOPEScanSig 8 defaultTiptopSigN8

boundsProgram "gny_getSpectrum_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ void $
  forConcurrently_ [ toV (1.06861, 0.6500, 1.725) ]
  (\pt -> remoteComputeWithSpectrum 10 $ minCT pt Nothing 2)

boundsProgram "gny_getSpectrum_ngroup4_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ void $
  forConcurrently_ [ toV (1.04356, 0.7578, 1.899) ]
  (\pt -> remoteComputeWithSpectrum 10 $ minCT pt Nothing 4)

boundsProgram "gny_getSpectrum_ngroup8_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ void $
  forConcurrently_ [ toV (1.02119, 0.8665, 2.002) ]
  (\pt -> remoteComputeWithSpectrum 10 $ minCT pt Nothing 8)

boundsProgram "gny_navgrid_ngroup8_nmax8" =
  gnyNavigatorGrid 8 8 (Just $ toV (-0.1661, -0.04041, -0.5, -0.855))
  (chunksOf 50 $ pts) --defaultTiptopSigN8.initialSearch)
  where
    pts = toV <$> [ (1.0211 + 0.00005*dx, 0.8665 + 0.0004*dy, 2.005 + 0.008*dz, dsp)
                  | dsp <- [3.6] , dx <- [-1..3], dy <- [-1..3], dz <- [-2..2]]
--  (chunksOf 50 $ pts) --defaultTiptopSigN8.initialSearch)
--  where
--    pts = toV <$> [ (1.0211 + 0.00015*dx, 0.8665 + 0.0012*dy, 2.005 + 0.025*dz, dsp)
--                  | dsp <- [3, 3.1 .. 4] , (dx,dy,dz) <- cfCube ]

--  where
--    pts :: [V 4 Rational]
--    pts = toV <$> [ (1.0211 + dpsi, 0.8665 + dsig, 2.002 + deps, sig')
--                  | (dsig, deps) <- [(0,0), (-0.002,0), (0.002,0), (0,-0.003), (0,0.003)]
--                  , sig' <- [3.1, 3.2 .. 4.1]
--                  , dpsi <- [-0.0003, 0, 0.0003]
--                  ]

-- boundsProgram "gny_tiptop_ngroup4_nmax8" =
--   gnyTiptopSearchOPEScanSig 2 defaultTiptopSigN4
-- 
-- boundsProgram "gny_tiptop_ngroup8_nmax8" =
--   gnyTiptopSearchOPEScanSig 2 defaultTiptopSigN8

boundsProgram "gny_navgrid_ngroup2_nmax6" =
  gnyNavigatorGrid 2 6 (Just $ toV (-0.258492, -0.123751, -0.515709, -0.807397))
  (chunksOf 50 $ pts) --defaultTiptopSigN8.initialSearch)
  where
    pts = toV <$> [ (1.06857 + 0.002*dx, 0.6500 + 0.002*dy, 1.725 + 0.005*dz, dep)
                  | dep <- [3.4, 3.5, 3.6, 3.7] , dx <- [-2..2], dy <- [-2..2], dz <- [-2..2]]

boundsProgram "gny_tiptop_eps_ngroup2_nmax10" = gnyTiptopSearchOPEScanEp 2 defaultTiptopSigN2
boundsProgram "gny_tiptop_eps_ngroup4_nmax10" = gnyTiptopSearchOPEScanEp 4 defaultTiptopSigN4
boundsProgram "gny_tiptop_eps_ngroup8_nmax10" = gnyTiptopSearchOPEScanEp 8 defaultTiptopSigN8
boundsProgram "gny_sensitivity_sig2_nmax10" = gnySensitivity SigPrime 2 10 (toV (1.06856466809,0.650242006375,1.72811680641,3.07184375376))
boundsProgram "gny_sensitivity_sig4_nmax10" = gnySensitivity SigPrime 4 10 (toV (1.04350565051,0.75781443362 ,1.90114947601,3.25364233628))
boundsProgram "gny_sensitivity_sig8_nmax10" = gnySensitivity SigPrime 8 10 (toV (1.02114863784,0.867547685933,2.01169232166,3.19075745493))
boundsProgram "gny_sensitivity_eps2_nmax10" = gnySensitivity EpsPrime 2 10 (toV (1.06807513662,0.65334141843,1.73451066651,3.20389794707))
boundsProgram "gny_sensitivity_eps4_nmax10" = gnySensitivity EpsPrime 4 10 (toV (1.04316633247,0.760464072999,1.90629431796,3.14108314514))
boundsProgram "gny_sensitivity_eps8_nmax10" = gnySensitivity EpsPrime 8 10 (toV (1.0211097389,0.86738729442,2.00349193036,3.16730018556))

boundsProgram p = unknownProgram p

data OpChoice = SigPrime | EpsPrime
  deriving (Show, Eq, Ord, Enum)

-- Compute gap sensitivity of island tip
gnySensitivity :: OpChoice -> Rational -> Int -> V 4 Rational -> Cluster ()
gnySensitivity opChoice ngroup nmax tipPoint =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition jobPartition) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine pointFromBound startingCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine pointFromBound (Just initialLambda)
  delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
     (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound)
  where
    -- setup:
    startingCheckpoint = case (opChoice, ngroup, nmax) of
      (EpsPrime, 2, 10) -> Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-06/zDmyY/Object_xI3b_nMn6FCSEa53H0QaNkkRphjMdrXg5tr-bC5k4Mc/ck"
      (EpsPrime, 4, 10) -> Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-06/KlTtl/Object_28sobfmmZ5XRy9MSM6KD025CR8mSujNBmEAyd7fVLOo/ck"
      (EpsPrime, 8, 10) -> Just "/expanse/lustre/scratch/mmitchell/temp_project/data/2024-06/InwqK/Object_scX5ERqfNu0SOJWNgIsASK4_xO0mw9y5j6QjXk2IAoQ/ck"
      _ -> Nothing
    (jobTime, jobType, jobMemory, jobPartition, solverPrecision) = case nmax of
      6  -> ( 8*hour, MPIJob 1 64, "128G", "shared", 768)
      10 -> (16*hour, MPIJob 1 64, "128G", "shared", 768) -- change 768 to 960 if this fails, esp for N=2
      _  -> error $ "No default job params for nmax = " ++ show nmax
    (initialLambda, opeEllipse) = case ngroup of
      2 -> (toV (-0.258492, -0.123751, -0.515709, -0.807397),
            GNYData.gny2Sig25bilinearMatNmax10)
      4 -> (toV (-0.2148, -0.07637, -0.49035, -0.84117), N4.nmax6opeEllipse2)
      8 -> (toV (0.1661, 0.04041, 0.5, 0.855), N8.nmax6DelaunayOpeEllipse)
      _ -> error $ "No default initialLambda for N = " ++ show ngroup
    -- delaunay search:
    delaunayConfig = defaultDelaunayConfig 30 200
    affine = AffineTransform
      { affineShift  = primePoint
      , affineLinear = toV ( toV (0.25, 0  )
                           , toV (0   ,0.2)
                           )
      }
    -- dimensions:
    (dPsi,dSig,dEps,dPrime) = fromV tipPoint
    deltaExts = toV (dPsi,dSig,dEps)
    primePoint = case opChoice of
      SigPrime -> toV (dPrime, 4.5)
      EpsPrime -> toV (dPrime, 4)
    -- actual spectrum stuff:
    pointFromBound = case opChoice of
      SigPrime -> deltaExtToV'
      EpsPrime -> deltaExtToVee
    bound v = Bound
      { boundKey = case opChoice of
          SigPrime -> gnyFeasibleSPP v deltaExts (Just initialLambda) ngroup nmax
          EpsPrime -> gnyFeasibleEPP v deltaExts (Just initialLambda) ngroup nmax
      , precision = 768
      , solverParams = (Params.jumpFindingParams nmax)
                       { precision = solverPrecision , procGranularity = 4 }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $ [ (primePoint, Nothing) ]
