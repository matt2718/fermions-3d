{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}


module Bounds.Fermions3d.GNYHeisenberg where

import           Blocks                     (BlockFetchContext, Coordinate (XT),
                                             CrossingMat, Delta (..),
                                             Derivative (..), TaylorCoeff (..),
                                             Taylors, unzipBlock, xEven, xOdd,
                                             xtTaylors, yEven, yOdd)
import qualified Blocks.Blocks3d            as B3d
import           Blocks.Blocks3d.Build      (block3dBuildLink)
import           Bootstrap.Bounds           (BoundDirection, DeltaRange,
                                             FourPointFunctionTerm, HasRep (..),
                                             OPECoefficient,
                                             OPECoefficientExternal, Spectrum,
                                             ThreePointStructure (..),
                                             boundDirSign, crossingMatrix,
                                             crossingMatrixExternal, derivsVec,
                                             listDeltas, map4pt,
                                             mapBlocksFreeVect, mapOps,
                                             opeCoeffGeneric_,
                                             opeCoeffIdentical_, runTagged2)
import qualified Bootstrap.Bounds           as Bounds
import           Bootstrap.Build            (FetchConfig (..),
                                             SomeBuildChain (..), noDeps)
import           Bootstrap.Math.FreeVect    (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect    as FV
import           Bootstrap.Math.HalfInteger (HalfInteger)
import qualified Bootstrap.Math.HalfInteger as HI
import           Bootstrap.Math.Linear      (toV)
import qualified Bootstrap.Math.Linear      as L
import           Bootstrap.Math.VectorSpace (zero, (*^))
import qualified Bootstrap.Math.VectorSpace as VS
import           Bounds.Fermions3d.GNY      (ChannelType (..), so3)
import qualified Bounds.Fermions3d.GNY      as GNY
import           Bounds.Scalars3d.ONRep     (ON3PtStruct (..), ON4PtStruct (..),
                                             ONRep (..), evalONBlock)
import           Control.Monad.IO.Class     (liftIO)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Binary                (Binary)
import           Data.Data                  (Typeable)
import           Data.Matrix.Static         (Matrix)
import           Data.Reflection            (Reifies, reflect)
import           Data.Tagged                (Tagged)
import           Data.Traversable           (for)
import           Data.Vector                (Vector)
import           GHC.Generics               (Generic)
import           GHC.TypeNats               (KnownNat)
import           Hyperion                   (Dict (..), Static (..), cPtr)
import           Hyperion.Bootstrap.Bound   (BuildInJob,
                                             SDPFetchBuildConfig (..),
                                             ToSDP (..), blockDir)
import           Linear.V                   (V)
import qualified SDPB

import           Bounds.Fermions3d.SUNTableaux

data ExternalOp s = Sig | Psi | Eps
  deriving (Show, Eq, Ord, Enum, Bounded)

data HRep n = HRep
  { hSUN    :: SUNRep n
  , hSU2    :: SUNRep 2
  , hU1     :: U1Rep
  , hParity :: Z2Rep
  }
  deriving (Show, Eq, Ord, Generic)

instance (KnownNat n) => GroupIrrep (HRep n)

data ExternalDims = ExternalDims
  { deltaSig :: Rational
  , deltaPsi :: Rational
  , deltaEps :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (B3d.ConformalRep Rational, HRep n) where
--   rep x = case x of
--     Sig -> (B3d.ConformalRep dSig 0,     HRep (SUNRep [] ) (SUNRep [2]) (U1Rep 0) Z2Odd )
--     Psi -> (B3d.ConformalRep dPsi (1/2), HRep (SUNRep [1]) (SUNRep [1]) (U1Rep 1) Z2Even)
--     Eps -> (B3d.ConformalRep dEps 0,     HRep (SUNRep [] ) (SUNRep [] ) (U1Rep 0) Z2Even)
--     where
--       ExternalDims { deltaSig = dSig, deltaPsi = dPsi, deltaEps = dEps} = reflect x

data GNYHeisenberg = GNYHeisenberg
  { externalDims :: ExternalDims
  , nGroup       :: Rational
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 4 Rational))
  -- | StressTensorOPEBound BoundDirection
  -- | GFFNavigator (Maybe (V 4 Rational))
  -- | ExternalOPEBound BoundDirection
  -- | ConservedCurrentOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

