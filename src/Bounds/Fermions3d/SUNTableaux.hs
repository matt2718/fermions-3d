{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DefaultSignatures   #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Bounds.Fermions3d.SUNTableaux
  ( GroupIrrep(..)
--   , LieGroupIrrep(..)
  , SUNRep(..)
  , U1Rep(..)
  , Z2Rep(..)
  ) where

import           Control.Monad            (guard)
import           GHC.Generics             
import           Data.List                (sort, nub)
import           Data.Proxy               (Proxy (..))
import           GHC.TypeNats             (KnownNat, natVal)
import           Linear.Vector            (liftU2)
import           Blocks                   (Block (..))
import           Bootstrap.Math.FreeVect  (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect  as FV

class (Eq r, Ord r) => GroupIrrep r where
  trivialRep         :: r
  dimension          :: r -> Int
  irrepProduct       :: r -> r -> [r]
  irrepProductUnique :: r -> r -> [r]

  default irrepProductUnique :: r -> r -> [r]
  irrepProductUnique r1 r2 = nub $ sort $ irrepProduct r1 r2

  -- for generics:
  default trivialRep :: (Generic r, GGroupIrrep (Rep r)) => r
  trivialRep = to gTrivialRep

  default dimension :: (Generic r, GGroupIrrep (Rep r)) => r -> Int
  dimension r = gDimension (from r)

  default irrepProduct :: (Generic r, GGroupIrrep (Rep r)) => r -> r -> [r]
  irrepProduct r1 r2 = to <$> gIrrepProduct (from r1) (from r2)

data Group3PtStruct r = Group3PtStruct r r r
  deriving (Eq, Ord)

class (GroupIrrep r, Eq s, Ord s) => Has4PtStruct r s where
  evalBlock :: (Fractional a, Eq a) => Block (Group3PtStruct r) s -> a
  
  -- for generics:
  default evalBlock :: ( Generic r, Generic s, GHas4pt (Rep r) (Rep s)
                       , Fractional a, Eq a )
                    => Block (Group3PtStruct r) s -> a
  evalBlock (Block (Group3PtStruct r1 r2 r) (Group3PtStruct r4 r3 r') f) =
    gEvalBlock (Block
                 (Group3PtStruct (from r1) (from r2) (from r))
                 (Group3PtStruct (from r4) (from r3) (from r'))
                 (from f)
               )

------------ GENERIC PLUMBING ------------

class GGroupIrrep f where
  gTrivialRep   :: f r
  gDimension    :: f r -> Int
  gIrrepProduct :: f r -> f r -> [f r]

instance (GGroupIrrep a, GGroupIrrep b) => GGroupIrrep (a :*: b) where
  gTrivialRep = gTrivialRep :*: gTrivialRep
  gDimension (ra :*: rb) = gDimension ra * gDimension rb
  gIrrepProduct (ra1 :*: rb1) (ra2 :*: rb2) =
    [ ra :*: rb | ra <- gIrrepProduct ra1 ra2, rb <- gIrrepProduct rb1 rb2 ]

instance (GGroupIrrep a) => GGroupIrrep (M1 i c a) where
  gTrivialRep = M1 gTrivialRep
  gDimension (M1 r) = gDimension r
  gIrrepProduct (M1 r1) (M1 r2) = M1 <$> gIrrepProduct r1 r2

instance (GroupIrrep a) => GGroupIrrep (K1 i a) where
  gTrivialRep = K1 trivialRep
  gDimension (K1 r) = dimension r
  gIrrepProduct (K1 r1) (K1 r2) = K1 <$> irrepProduct r1 r2

class GHas4pt fr fs where
  gEvalBlock :: (Fractional a, Eq a) => Block (Group3PtStruct (fr r)) (fs s) -> a

instance (GHas4pt a sa, GHas4pt b sb) => GHas4pt (a :*: b) (sa :*: sb) where
  gEvalBlock (Block
               (Group3PtStruct (r1a :*: r1b) (r2a :*: r2b) (ra  :*: rb ))
               (Group3PtStruct (r4a :*: r4b) (r3a :*: r3b) (ra' :*: rb'))
               (fa :*: fb)
             )
    = gEvalBlock (Block (Group3PtStruct r1a r2a ra) (Group3PtStruct r4a r3a ra') fa) *
      gEvalBlock (Block (Group3PtStruct r1b r2b rb) (Group3PtStruct r4b r3b rb') fb)

instance (GHas4pt a s) => GHas4pt (M1 ia ca a) (M1 is cs s) where
  gEvalBlock (Block
               (Group3PtStruct (M1 r1) (M1 r2) (M1 r))
               (Group3PtStruct (M1 r4) (M1 r3) (M1 r'))
               (M1 f)
             )
    = gEvalBlock (Block (Group3PtStruct r1 r2 r) (Group3PtStruct r4 r3 r') f)

-- instance (GHas4pt a s) => GHas4pt (K1 ia a) (K1 is s) where
--   gEvalBlock (Block
--                (Group3PtStruct (K1 r1) (K1 r2) (K1 r))
--                (Group3PtStruct (K1 r4) (K1 r3) (K1 r'))
--                (K1 f)
--              )
--     = gEvalBlock (Block (Group3PtStruct r1 r2 r) (Group3PtStruct r4 r3 r') f)


-- instance GGroupIrrep U1 where
--   gIrrepProduct _ _ = []
-- 
-- instance (GGroupIrrep a, GGroupIrrep b) => GGroupIrrep (a :+: b) where
--   gIrrepProduct (L1 ra1) (L1 ra2) = L1 <$> gIrrepProduct ra1 ra2
--   gIrrepProduct (R1 rb1) (R1 rb2) = R1 <$> gIrrepProduct rb1 rb2
--   gIrrepProduct _ _ = []

------------ SPECIFIC GROUPS ------------

data SUNRep n = SUNRep [Int]
  deriving (Show, Eq, Ord)

instance (KnownNat n) => GroupIrrep (SUNRep n) where
  trivialRep = SUNRep []

  dimension r | r == trivialRep = 1
              | otherwise
    = foldr1 (*) (concat factors) `div` foldr1 (*) (concat (youngHooks tab))
    where
      factors = zipWith (\r width -> take width [(n'-r)..]) [0..] tab
      SUNRep tab = sunReduce r
      n' = fromIntegral $ natVal @n Proxy

  irrepProduct (SUNRep tab1) (SUNRep tab2) =
    (sunReduce . SUNRep) <$> youngProduct n' tab1 tab2
    where n' = fromIntegral $ natVal @n Proxy

data U1Rep = U1Rep Int
  deriving (Show, Eq, Ord)

instance GroupIrrep U1Rep where
  trivialRep = U1Rep 0
  dimension _ = 1
  irrepProduct (U1Rep p) (U1Rep q) = [U1Rep (p + q)]

data Z2Rep = Z2Even | Z2Odd
  deriving (Show, Eq, Ord, Enum)

instance GroupIrrep Z2Rep where
  trivialRep = Z2Even
  dimension _ = 1
  irrepProduct r1 r2 = [if r1 == r2 then Z2Even else Z2Odd]

---------- TENSOR STRUCTURES ----------

data SUN4ptStruct n = QPlus | QMinus | Q3 | Unique
  deriving (Eq, Ord)

---------- UTILITY FUNCTIONS ----------

-- | Checks whether a given list descreases monotonically
isMonoDecreasing :: Ord a => [a] -> Bool
isMonoDecreasing l = all id $ zipWith (>=) l (tail l)

-- | Puts an SU(N) irrep into standard form (with validation)
sunReduce :: forall n . KnownNat n => SUNRep n -> SUNRep n
sunReduce rep@(SUNRep tab) =
  if length tab' <= n' && isMonoDecreasing tab' then
    if length tab' == n' then
      SUNRep $ dropTrailingZeros $ subtract (last tab') <$> tab'
    else
      SUNRep tab'
  else
    error ("Invalid representation: " ++  show rep)
  where
    n' = fromIntegral $ natVal @n Proxy
    dropTrailingZeros = reverse . dropWhile (==0) . reverse
    tab' = dropTrailingZeros tab

-- | Product of two young tableaux, following the conventions of Georgi sections
-- 12.2 and 13.2. Irreps that appear multiple times in the product will appear
-- multiple times in the result. Some tableaux may be returned with leading
-- columns of size n.
youngProduct :: Int -> [Int] -> [Int] -> [[Int]]
youngProduct n tab1 tab2 = map length <$> go fullTab1 toAdd
  where
    fullTab1 = flip replicate Nothing <$> tab1
    toAdd = zip tab2 [0..]
    go :: [[Maybe Int]] -> [(Int,Int)] -> [[[Maybe Int]]]
    go t1 [] = [t1]
    go t1 ((m,val):rows) = do
      toAdd <- sowBlocks <$> whereToAdd (length t1) m []
      let tNew = liftU2 (<>) t1 (flip replicate (Just val) <$> toAdd)
      guard $ length tNew <= n
      guard $ isMonoDecreasing (length <$> tNew)
      guard $ val <= 0 || boxCondition (val-1) val tNew
      go tNew rows
    -- Distinct ways to add m blocks to a young tableau of height h, where
    -- prevAdded have already been added. Returns a list of monotonically
    -- increasing lists of the (0-indexed) rows we add each block to.
    whereToAdd _      m prevAdded | m <= 0 = [reverse prevAdded]
    whereToAdd height m prevAdded = do
      let lastIdx = if prevAdded == [] then 0 else head prevAdded
          maxIdx  = max (lastIdx+1) height
      next <- [lastIdx .. maxIdx]
      whereToAdd height (m-1) (next:prevAdded)
    -- given a monotonically increasing list of rows to add blocks to, return
    -- a list of the number of blocks added in each row
    sowBlocks toSow = reverse $ sowBlocks' 0 toSow [0]
    sowBlocks' :: Int -> [Int] -> [Int] -> [Int]
    sowBlocks' _ [] pastRows = pastRows
    sowBlocks' curIdx toSow@(x:xs) sownRows@(r:rs)
      | curIdx <  x = sowBlocks' (curIdx+1) toSow (0:sownRows)
      | curIdx == x = sowBlocks'  curIdx    xs    ((r+1):rs)
      | otherwise = undefined
    -- see section 12.2 of Georgi
    boxCondition a b = all (>=0) . scanl1 (+) . map toCount . concat . map reverse
      where
        toCount x | x == Just a =  1
                  | x == Just b = -1
                  | otherwise   = 0

-- | Returns a list of the hooks in a young tableau, arranged spacially
youngHooks :: [Int] -> [[Int]]
youngHooks [] = []
youngHooks (width:remaining) = hooksInRow : youngHooks remaining
  where
    hooksInRow = map (\x -> (width-x) + countWhile (>x) remaining) [0..width-1]

-- | Length of an initial prefix in the list satisfying p
countWhile :: (a -> Bool) -> [a] -> Int
countWhile p = go 0
  where go !n (x:xs) | p x = go (n+1) xs
        go !n _            = n
