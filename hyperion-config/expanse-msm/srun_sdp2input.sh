#!/bin/bash

module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7

echo srun -v --kill-on-bad-exit=1 --mpi=pmi2 /home/wlandry/gnu_openmpi/install/bin/sdp2input $@
srun -v --kill-on-bad-exit=1 --mpi=pmi2 /home/wlandry/gnu_openmpi/install/bin/sdp2input $@
