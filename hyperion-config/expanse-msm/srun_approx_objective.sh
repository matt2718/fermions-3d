#!/bin/bash

module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7
# module load CMake GCC Boost Eigen METIS
echo srun /home/wlandry/gnu_openmpi/install/bin/approx_objective $@
srun /home/wlandry/gnu_openmpi/install/bin/approx_objective $@

