#!/bin/bash

# non-dynamic openblas???
module load cpu/0.15.4 gcc/10.2.0 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0
# module load openblas

echo srun /home/mmitchell/bin/dynamical_sdp_RV1B $@
srun /home/mmitchell/bin/dynamical_sdp_RV1B $@
