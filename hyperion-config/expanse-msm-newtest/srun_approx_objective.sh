#!/bin/bash

module load cpu gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7

echo srun /home/vdommes/install/sdpb-bigint-syrk-blas/bin/approx_objective $@
srun /home/vdommes/install/sdpb-bigint-syrk-blas/bin/approx_objective $@
#echo srun /home/wlandry/gnu_openmpi/install/bin/approx_objective $@
#srun /home/wlandry/gnu_openmpi/install/bin/approx_objective $@
