#!/bin/bash
#SBATCH --job-name="spectrum"
#SBATCH --output="/home/aikeliu/stress_spec/nmax6_test/spectrum-test-%j.out"
#SBATCH --no-requeue
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=128
#SBATCH --mem=0G
#SBATCH --account=yun124
#SBATCH -t 12:00:00


module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm

echo srun -v -t 12:00:00 --partition=compute --account=yun124 --nodes=1 --ntasks-per-node=64 /home/vdommes/install/sdpb-bigint-syrk-blas/bin/spectrum $@
srun -v  -t 12:00:00 --partition=compute --account=yun124 --nodes=1 --ntasks-per-node=64 /home/vdommes/install/sdpb-bigint-syrk-blas/bin/spectrum $@

